package com.example.demo.repository;

import com.example.demo.feature.users.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    long count();
    User findByPesel(Long pesel);
}