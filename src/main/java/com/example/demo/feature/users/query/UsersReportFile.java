package com.example.demo.feature.users.query;

import com.example.demo.feature.users.entity.User;
import com.example.demo.utils.exception.NoFoundException;
import com.example.demo.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UsersReportFile {

    private final UserRepository userRepository;
    private final AllUsers allUsers;
    private final ObjectMapper objectMapper;

    public File saveUsersToFile() {
        long usersCount = userRepository.count();
        if (usersCount == 0) {
            log.info("USERS NOT FOUND : saveUsersToFile");
            throw new NoFoundException("Users not found");
        }

        List<User> users = allUsers.getAllUsers();
        File file = new File("users.json");
        try {
            objectMapper.writeValue(file, users);
            log.info("FILE CREATED : saveUsersToFile");
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            log.info("Error IOException : saveUsersToFile");
            return new File("error");
        }
    }
}
