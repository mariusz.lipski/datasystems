package com.example.demo.repository;

import com.example.demo.feature.communicationMethod.CommunicationMethod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommunicationMethodRepository extends JpaRepository<CommunicationMethod, Long> {
}
