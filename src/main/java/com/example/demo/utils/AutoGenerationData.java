package com.example.demo.utils;

import com.example.demo.feature.communicationMethod.CommunicationMethod;
import com.example.demo.feature.users.entity.User;
import com.example.demo.repository.CommunicationMethodRepository;
import com.example.demo.repository.UserRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Random;

@Component
public class AutoGenerationData {
    @Bean
    public CommandLineRunner demo(UserRepository userRepository, CommunicationMethodRepository communicationMethodRepository) {
        return (args) -> {

            Faker faker = new Faker(new Locale("pl"));
            Random random = new Random();
            for(int i=0; i<15000; i++){
                User user = new User();
                user.setFirstName(faker.name().firstName());
                user.setLastName(faker.name().lastName());
                user.setPesel(Long.valueOf(faker.number().digits(11)));
                userRepository.save(user);

                int communicationMethodsCount = random.nextInt(3) + 2 ; // generuje liczbę od 2 do 4
                for(int j=0; j<communicationMethodsCount; j++) {
                    CommunicationMethod communicationMethod = new CommunicationMethod();
                    communicationMethod.setEmail(faker.internet().emailAddress());
                    communicationMethod.setResidenceAddress(faker.address().fullAddress());
                    communicationMethod.setRegistrationAddress(faker.address().fullAddress());
                    communicationMethod.setPrivatePhoneNumber(faker.phoneNumber().phoneNumber());
                    communicationMethod.setWorkPhoneNumber(faker.phoneNumber().cellPhone());
                    communicationMethod.setUser(user);
                    communicationMethodRepository.save(communicationMethod);
                }
            }
        };
    }

}
