package com.example.demo.feature.users.query;

import com.example.demo.feature.users.entity.User;
import com.example.demo.utils.exception.IllegalArgumentException;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class FindUserByPesel {

    private final UserRepository userRepository;

    public Optional<User> findUserByPesel(Long pesel) {
        try {
            check(pesel);
            return Optional.ofNullable(userRepository.findByPesel(pesel));
        } catch (Exception e) {
            log.info("Error findUserByPesel : " + e.getMessage());
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private void check(Long pesel) {
        if (pesel == null || pesel == 0) {
            throw new IllegalArgumentException("Pesel can't null or 0");
        }
        if (!hasElevenDigits(pesel)) {
            throw new IllegalArgumentException("Pesel must have 11 digits");
        }

    }

    private boolean hasElevenDigits(Long number) {
        return Long.toString(number).length() == 11;
    }

}