package com.example.demo.feature.users.entity;

import com.example.demo.feature.communicationMethod.CommunicationMethod;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "\"user\"", indexes = {@Index(name = "pesel_index", columnList = "pesel", unique = true)}, uniqueConstraints = {@UniqueConstraint(columnNames = {"pesel"})})
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    @Column(unique=true)
    private Long pesel;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<CommunicationMethod> communicationMethods;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", pesel='" + pesel + '\'' +
                ", communicationMethodsCount=" + (communicationMethods != null ? communicationMethods.size() : 0) +
                '}';
    }
}
