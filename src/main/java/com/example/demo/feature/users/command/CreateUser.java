package com.example.demo.feature.users.command;

import com.example.demo.feature.users.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.utils.exception.IllegalArgumentException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CreateUser {

    private final UserRepository userRepository;

    @Transactional
    public User createUser(User user) {
        validation(user);
        User savedUser = userRepository.save(user);
        log.info("USER CREATED : {}", savedUser);
        return savedUser;
    }

    private void validation(User user){
        if(user == null){
            throw new IllegalArgumentException("User can't be null");
        }
        if(user.getPesel() == null || user.getPesel() == 0){
            throw new IllegalArgumentException("Pesel can't be null");
        }
        if(hasNineDigits(user.getPesel())){
            throw new IllegalArgumentException("Pesel must have 9 digits");
        }
        if(user.getFirstName() == null || user.getFirstName().isEmpty()){
            throw new IllegalArgumentException("First name can't be null");
        }
        if(user.getLastName() == null || user.getLastName().isEmpty()){
            throw new IllegalArgumentException("Last name can't be null");
        }
        if (userRepository.findByPesel(user.getPesel()) != null) {
            throw new IllegalArgumentException("User with this pesel already exists");
        }

    }

    private boolean hasNineDigits(Long number) {
        return Long.toString(number).length() == 9;
    }
}
