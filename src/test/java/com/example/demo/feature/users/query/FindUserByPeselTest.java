package com.example.demo.feature.users.query;

import com.example.demo.feature.users.entity.User;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class FindUserByPeselTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FindUserByPesel findUserByPesel;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findUserByPeselTest() {
        // given
        Long pesel = 12345678901L;
        User user = new User();
        user.setPesel(pesel);
        when(userRepository.findByPesel(pesel)).thenReturn(user);

        // when
        Optional<User> foundUser = findUserByPesel.findUserByPesel(pesel);

        // then
        assertTrue(foundUser.isPresent());
        assertEquals(user.getPesel(), foundUser.get().getPesel());
        verify(userRepository, times(1)).findByPesel(pesel);
    }

    @Test
    public void findUserByPeselNotFoundTest() {
        // given
        Long pesel = 12345678901L;
        when(userRepository.findByPesel(pesel)).thenReturn(null);

        // when
        Optional<User> foundUser = findUserByPesel.findUserByPesel(pesel);

        // then
        assertFalse(foundUser.isPresent());
        verify(userRepository, times(1)).findByPesel(pesel);
    }

    @Test
    public void findUserByPeselExceptionTest() {
        // given
        Long pesel = 12345678901L;
        when(userRepository.findByPesel(pesel)).thenThrow(new RuntimeException("Database error"));

        // when
        Optional<User> foundUser = findUserByPesel.findUserByPesel(pesel);

        // then
        assertFalse(foundUser.isPresent());
        verify(userRepository, times(1)).findByPesel(pesel);
    }


}