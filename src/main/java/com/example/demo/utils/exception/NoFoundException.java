package com.example.demo.utils.exception;

public class NoFoundException extends RuntimeException{

    public NoFoundException(String message) {
        super(message);
    }
}
