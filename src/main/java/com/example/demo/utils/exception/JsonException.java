package com.example.demo.utils.exception;

public class JsonException extends RuntimeException{

    public JsonException(String message) {
        super(message);
    }
}
