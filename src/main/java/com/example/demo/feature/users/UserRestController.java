package com.example.demo.feature.users;

import com.example.demo.feature.users.command.CreateUser;
import com.example.demo.feature.users.query.AllUsers;
import com.example.demo.feature.users.query.FindUserByPesel;
import com.example.demo.feature.communicationMethod.CommunicationMethod;
import com.example.demo.feature.users.entity.User;
import com.example.demo.repository.CommunicationMethodRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.feature.users.query.UsersReportFile;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserRestController {

    private final UserRepository userDtoRepository;
    private final CommunicationMethodRepository communicationMethodRepository;
    private final UsersReportFile usersReportFile;
    private final FindUserByPesel findUserByPesel;
    private final AllUsers allUsers;
    private final CreateUser createUser;

    @PostMapping("/users/{userId}/communication-methods")
    public CommunicationMethod addCommunicationMethod(@PathVariable Long userId, @RequestBody CommunicationMethod communicationMethod) {
        User user = userDtoRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
        communicationMethod.setUser(user);
        return communicationMethodRepository.save(communicationMethod);
    }

    @PostMapping("/")
    public ResponseEntity<User> createUserDto(@RequestBody User user) {
        for (CommunicationMethod method : user.getCommunicationMethods()) {
            method.setUser(user);
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(createUser.createUser(user));
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> getAlluserDtos() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(allUsers.getAllUsers());
    }

    @GetMapping("/peselNumber/{pesel}")
    public ResponseEntity<Optional<User>> getUserDtoByPesel(@PathVariable Long pesel) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(findUserByPesel.findUserByPesel(pesel));}

    @GetMapping("/saveToFile")
    public ResponseEntity<String> saveUsersToFile() {
        File file = usersReportFile.saveUsersToFile();
        if (file.getName().equals("error")) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Błąd zapisu do pliku");
        }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Zapisano użytkowników do pliku");
    }

}