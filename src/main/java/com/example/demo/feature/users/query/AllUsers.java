package com.example.demo.feature.users.query;

import com.example.demo.feature.users.entity.User;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AllUsers {

    private final UserRepository userRepository;

    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            log.info("USERS NOT FOUND : getAllUsers");
            return List.of();
        }
        log.info("USERS FOUND : getAllUsers");
        return users;
    }
}