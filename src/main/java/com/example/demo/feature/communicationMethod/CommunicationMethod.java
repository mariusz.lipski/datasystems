package com.example.demo.feature.communicationMethod;
import com.example.demo.feature.users.entity.User;
import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class CommunicationMethod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String residenceAddress;
    private String registrationAddress;
    private String privatePhoneNumber;
    private String workPhoneNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Override
    public String toString() {
        return  "email='" + email + '\'' +
                ", residenceAddress='" + residenceAddress + '\'' +
                ", registrationAddress='" + registrationAddress + '\'' +
                ", privatePhoneNumber='" + privatePhoneNumber + '\'' +
                ", workPhoneNumber='" + workPhoneNumber + '\'' +
                ", userId=" + (user != null ? user.getId() : null) ;
    }

}