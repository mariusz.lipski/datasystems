package com.example.demo.utils.exception;

public class SQLDatabaseException extends RuntimeException{

    public SQLDatabaseException(String message) {
        super(message);
    }
}
